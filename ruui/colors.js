export const colors = {
  blue346fef: '#346fef',
  grayf4f4f4: '#f4f4f4',
  mainRight: '#fff',
  gray767678: '#767678',
  blue005A9E: '#005A9E',
  blueC3ddf7: '#c3ddf7',
  redDA532C: '#da532c',
  whiteFAF9F8: '#faf9f8',
  green009900: '#009900',
  grayE1DFDD: '#E1DFDD',
};
