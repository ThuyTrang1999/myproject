import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import TitleMainRight from '../../../component/itemMain/titleMainRight';
import {assets} from '../../../../ruui/assets';
import {colors} from '../../../../ruui/colors';
import TitleMainTextRight from '../../../Component/itemMain/titleMainTextRight';

type Props = {};
export default function WasPlanning(props: Props) {
  let today = new Date();
  let date =
    today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
  let time =
    today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
  let dateTime = date + ' ' + time;
  const [postList, setPostList] = useState([]);
  useEffect(() => {
    async function fetchPostList() {
      try {
        const requestUrl =
          'http://js-post-api.herokuapp.com/api/posts?_limit=10&_page=1';
        const response = await fetch(requestUrl);
        const responseJSON = await response.json();
        console.log(responseJSON);

        const {data} = responseJSON;
        setPostList(data);
      } catch (error) {
        console.log('Failed to fetch post list', error.message);
      }
    }
    fetchPostList();
  }, []);

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', flex: 1}}>
        <View style={{flex: 4}}>
          <TitleMainRight title={'Đã lập kế hoạch'} img={assets.dot} />
          <Text style={styles.styleText}>{dateTime}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          <TitleMainTextRight img={assets.light} title={'Hôm nay'} />
        </View>
      </View>
      <View
        style={{
          flex: 5,
          paddingVertical: 30,
          flexDirection: 'row',
        }}>
        <View style={{flex: 2}}>
          {postList.map((post) => (
            <Text key={post.id}>{post.title}</Text>
          ))}
        </View>
        <View style={{flex: 4}}>
          <Text>1256</Text>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: colors.mainRight,
    // flexDirection: 'row',
    flex: 1,
  },
  styleText: {
    marginTop: 10,
    fontSize: 12,
  },
  styleImg: {
    width: 15,
    height: 15,
    marginLeft: 20,
    marginTop: 5,
  },
});
