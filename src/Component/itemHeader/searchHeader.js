import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Text,
  Dimensions,
  FlatList,
} from 'react-native';
import {assets} from '../../../ruui/assets';
import {colors} from '../../../ruui/colors';
import Hover from '../../Component/hover';
import {TouchableOpacity} from 'react-native-web';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'modal-react-native-web';

type Props = {};
let arrSearch = [];
let dataSearched = [];
export default function SearchHeader(props: Props) {
  const [placeholder, setPlaceholder] = useState('');
  const [searchState, setSearchState] = useState('');
  const [state, setState] = useState(false);
  const getItem = async () => {
    await AsyncStorage.getItem('list', (err, result) => {
      arrSearch = JSON.parse(result);
    });
  };
  const windowsWidth = Dimensions.get('window').width;
  const windowsHeight = Dimensions.get('window').height;
  const [modalVisible, setmodalVisible] = useState(false);
  const [modalVisibles, setmodalVisibles] = useState('');
  const hideDialog = () => {
    setmodalVisible(false);
  };
  getItem();
  return (
    <View>
      <Hover>
        {({focused}) => {
          return (
            <View
              style={[
                styles.searchHeader,
                {
                  backgroundColor: focused
                    ? colors.mainRight
                    : colors.blueC3ddf7,
                },
              ]}>
              <View style={{paddingHorizontal: 10}}>
                <TouchableOpacity
                  onPress={() => {
                    // setmodalVisible(true);

                    let searchT = searchState;
                    arrSearch.map((i) => {
                      if (i.text === searchT) {
                        dataSearched.push(i);
                        // dataSearched = arrSearch.filter((item) => {
                        //   item.text === searchState;
                        // });
                      }
                    });
                    console.log(dataSearched);
                    // }
                    AsyncStorage.setItem(
                      'listSearch',
                      JSON.stringify(
                        arrSearch.filter((item) => {
                          item.text === searchState;
                        }),
                      ),
                      () => {
                        let f = !state.schedules;
                        // console.log(arrSearch);
                        setState({
                          schedules: f,
                        });
                      },
                    );
                    // console.log(searchT);
                  }}>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                    }}
                    source={assets.search}
                  />
                </TouchableOpacity>
              </View>
              <TextInput
                style={styles.textInputSearch}
                placeholder={placeholder}
                onFocus={() => setPlaceholder('Tìm kiếm')}
                onBlur={() => setPlaceholder('')}
                value={searchState}
                onChangeText={(search) => {
                  setSearchState(search);
                }}
              />
            </View>
          );
        }}
      </Hover>
      <Modal visible={modalVisible} animationType="fade" transparent={true}>
        <View
          style={{
            width: windowsWidth,
            height: windowsHeight,
            backgroundColor: '#000',
            opacity: 0.1,
            margin: 'auto',
            position: 'absolute',
          }}
        />
        <View style={styles.viewModal}>
          <Text
            style={{
              alignSelf: 'center',
              fontWeight: 'bold',
              fontSize: 30,
            }}>
            Search
          </Text>
          <FlatList
            data={dataSearched}
            keyExtractor={(item, index) => 'key' + index}
            renderItem={({item}) => {
              return <Text>{item.text}</Text>;
            }}
          />
          <TouchableOpacity
            onPress={() => {
              hideDialog();
            }}
            style={styles.btnEdit}
            activeOpacity={1}>
            <Text style={{color: colors.mainRight}}>Thoát</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  );
}
const styles = StyleSheet.create({
  searchHeader: {
    flex: 1,
    width: 400,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: colors.blueC3ddf7,
    borderWidth: 1,
    borderRadius: 4,
    marginVertical: 10,
    padding: 5,
  },
  textInputSearch: {
    width: '100%',
  },
  viewModal: {
    marginHorizontal: 400,
    marginVertical: 200,
    backgroundColor: colors.mainRight,
    padding: 30,
  },
  btnEdit: {
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: colors.blueC3ddf7,
    padding: 10,
    marginTop: 20,
    backgroundColor: 'red',
    shadowColor: '#c6c2c2',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
});
