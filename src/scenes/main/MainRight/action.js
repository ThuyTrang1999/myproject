import React, {useState} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import TitleMainRight from '../../../Component/itemMain/titleMainRight';
import {assets} from '../../../../ruui/assets';
import {colors} from '../../../../ruui/colors';
import TitleMainTextRight from '../../../Component/itemMain/titleMainTextRight';
import Hover from '../../../Component/hover';
import NotiModal from '../../../Component/NotiModal';
import {ActionContext} from '../../../res/DataContext';

type Props = {};
export default function Action(props: Props) {
  const [editItem, setEditItem] = useState(null),
    [stateCont, setStateCont] = useState(
      localStorage.getItem('listContext')
        ? JSON.parse(localStorage.getItem('listContext'))
        : [],
    );
  const [modalVisible, setModalVisible] = useState(false);
  React.useEffect(() => {
    localStorage.setItem('listContext', JSON.stringify(stateCont));
  }, [stateCont]);
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row', flex: 1}}>
        <View style={{flex: 4}}>
          <TitleMainRight title={'Tác vụ'} img={assets.dot} />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          <TitleMainTextRight img={assets.light} title={'Hôm nay'} />
          <Hover>
            {({focused}) => {
              return (
                <View
                  style={[
                    styles.styleHover,
                    {backgroundColor: focused ? colors.grayf4f4f4 : '#fff'},
                  ]}>
                  <TouchableOpacity style={{flexDirection: 'row', padding: 8}}>
                    <Image source={assets.sort} style={styles.styleImg} />
                    <Text style={{alignSelf: 'center'}}>Sắp xếp </Text>
                  </TouchableOpacity>

                  {/*<TitleMainRight img={assets.light} title={'Hôm nay'} />*/}
                </View>
              );
            }}
          </Hover>
        </View>
      </View>
      <ActionContext.Provider
        value={{
          contextEdit: [editItem, setEditItem],
          contextList: [stateCont, setStateCont],
          contextModal: [modalVisible, setModalVisible],
        }}>
        <NotiModal />
      </ActionContext.Provider>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: colors.mainRight,
    // flexDirection: 'row',
    flex: 1,
  },
  styleText: {
    marginTop: 10,
    fontSize: 12,
  },
  styleImg: {
    width: 15,
    height: 15,
    marginLeft: 10,
    marginTop: 5,
  },
});
