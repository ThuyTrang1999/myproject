import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {assets} from '../../../ruui/assets';
import {TouchableOpacity} from 'react-native-web';
import {colors} from '../../../ruui/colors';
import Hover from '../../Component/hover';
import {DropdownContainer} from 'react-universal-ui';
import DropdownMenuHeader from './dropdownMenuHeader';

type Props = {};
export default function LogoHeader(props: Props) {
  return (
    <View style={styles.logoHeader}>
      <DropdownContainer
        dropdownComponent={DropdownMenuHeader}
        dropdownOffset={{right: 100}}>
        <Hover>
          {({focused}) => {
            return (
              <View
                style={{
                  padding: 15,
                  backgroundColor: focused ? colors.blue005A9E : null,
                }}>
                <TouchableOpacity activeOpacity={1}>
                  <Image
                    style={{
                      width: 20,
                      height: 20,
                    }}
                    source={assets.menuHeader}
                  />
                </TouchableOpacity>
              </View>
            );
          }}
        </Hover>
      </DropdownContainer>

      <View>
        <TouchableOpacity activeOpacity={1}>
          <Text style={styles.toDo}> To Do</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  logoHeader: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  toDo: {
    color: colors.mainRight,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
