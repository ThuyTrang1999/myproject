import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import Hover from '../../Component/hover';
import {TouchableOpacity} from 'react-native-web';
import {colors} from '../../../ruui/colors';
import {assets} from '../../../ruui/assets';
import ItemDropDownMenuHeader from '../itemHeader/itemDropDownMenuHeader';
type Props = {
  close?: Function,
};
export default function DropdownMenuHeader(props: Props) {
  const {close} = props;
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <View>
          <Hover>
            {({focused}) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    close();
                  }}
                  style={{
                    backgroundColor: focused ? colors.grayE1DFDD : null,
                  }}
                  activeOpacity={1}>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                      margin: 10,
                    }}
                    source={assets.menuHeaderBlack}
                  />
                </TouchableOpacity>
              );
            }}
          </Hover>
        </View>
        <View
          style={{marginLeft: 150, flexDirection: 'row', paddingVertical: 10}}>
          <Hover>
            {({focused}) => {
              return (
                <TouchableOpacity
                  style={{
                    color: colors.blue005A9E,
                    fontWeight: 'bold',
                  }}
                  activeOpacity={1}>
                  Office
                </TouchableOpacity>
              );
            }}
          </Hover>
          <Hover>
            {({focused}) => {
              return (
                <TouchableOpacity
                  style={{
                    color: colors.blue005A9E,
                  }}
                  activeOpacity={1}>
                  <Image
                    style={{
                      width: 20,
                      height: 20,
                      marginLeft: 10,
                    }}
                    source={assets.arrow}
                  />
                </TouchableOpacity>
              );
            }}
          </Hover>
        </View>
      </View>
      <Text style={styles.microsoft}>Microsoft365</Text>
      <ItemDropDownMenuHeader
        img={assets.outlook}
        text={'Outlook'}
        img1={assets.onedrive}
        text1={'OneDrive'}
      />
      <ItemDropDownMenuHeader
        img={assets.word}
        text={'Word'}
        img1={assets.excel}
        text1={'Excel'}
      />
      <ItemDropDownMenuHeader
        img={assets.powerpoint}
        text={'Powerpoint'}
        img1={assets.date}
        text1={'Date'}
      />

      <ItemDropDownMenuHeader
        img={assets.skype}
        text={'Skype'}
        img1={assets.msn}
        text1={'Msn'}
      />
      <ItemDropDownMenuHeader
        img={assets.office}
        text={'Office'}
        img1={assets.form}
        text1={'Form'}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 300,
    backgroundColor: colors.whiteFAF9F8,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  dropdownText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  microsoft: {
    paddingVertical: 18,
    fontSize: 23,
    fontWeight: 500,
  },
});
