import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import Hover from '../../Component/hover';
import {ScrollView, TouchableOpacity} from 'react-native-web';
import {colors} from '../../../ruui/colors';
import {assets} from '../../../ruui/assets';
type Props = {
  close?: Function,
};
export default function DropdownQuestion(props: Props) {
  const {close} = props;
  return (
    <View style={[styles.container, {maxHeight: 600, width: 250}]}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text style={styles.dropdownText}>Trợ giúp</Text>
        <View>
          <Hover>
            {({focused}) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    close();
                  }}
                  style={{
                    backgroundColor: focused ? colors.grayE1DFDD : null,
                  }}
                  activeOpacity={1}>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                      margin: 10,
                      alignContent: 'flex-end',
                    }}
                    source={assets.delete}
                  />
                </TouchableOpacity>
              );
            }}
          </Hover>
        </View>
      </View>
      <TouchableOpacity
        style={{marginTop: 15, color: colors.blue005A9E}}
        activeOpacity={1}>
        Nhận hỗ trợ
      </TouchableOpacity>
      <TouchableOpacity
        style={{marginTop: 10, color: colors.blue005A9E}}
        activeOpacity={1}>
        Đề xuất tính năng
      </TouchableOpacity>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity style={styles.btnDongBo} activeOpacity={1}>
          Đồng bộ
        </TouchableOpacity>
        <Text style={styles.textCapNhat}>Cập nhật</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 400,
    backgroundColor: colors.whiteFAF9F8,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  btnDongBo: {
    width: 87,
    height: 30,
    flexDirection: 'row',
    backgroundColor: colors.blue346fef,
    borderRadius: 3,
    color: colors.mainRight,
    fontWeight: 'bold',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 15,
  },
  dropdownText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  imgDelete: {
    alignContent: 'flex-end',
    // marginLeft: 160,
  },
  textCapNhat: {
    alignSelf: 'center',
    padding: 15,
  },
});
