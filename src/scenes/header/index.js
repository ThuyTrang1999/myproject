import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {colors} from '../../../ruui/colors';
import SearchHeader from '../../Component/itemHeader/searchHeader';
import LogoHeader from '../../Component/itemHeader/logoHeader';
import AccountHeader from '../../Component/itemHeader/accountHeader';
type Props = {};
export default function Header(props: Props) {
  return (
    <View style={styles.container}>
      <LogoHeader />
      <SearchHeader />
      <AccountHeader />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.blue346fef,
    flexDirection: 'row',
  },
});
