import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import type {Style} from '../../typeDefinition';
type Props = {
  title?: string,
  img?: string,
  titleColor?: Style,
};
export default function TitleMainRight(props: Props) {
  const {title, img, titleColor} = props;
  return (
    <View>
      <TouchableOpacity activeOpacity={1} style={styles.container}>
        <Text style={[styles.styleTitle, titleColor]}>{title}</Text>
        <Image source={img} style={styles.styleImg} />
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  styleTitle: {
    fontSize: 20,
    fontWeight: 500,
  },
  styleImg: {
    width: 15,
    height: 15,
    marginLeft: 20,
    marginTop: 5,
  },
});
