import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors} from '../../../../ruui/colors';
type Props = {};
export default function MainRight(props: Props) {
  return <View style={styles.container} />;
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.mainRight,
    flexDirection: 'row',
  },
  styleText: {
    paddingHorizontal: 20,
    fontSize: 12,
  },
});
