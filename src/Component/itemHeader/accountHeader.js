import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-web';
import {assets} from '../../../ruui/assets';
import Hover from '../../Component/hover';
import {colors} from '../../../ruui/colors';
import ItemHeader from '../itemHeader/itemHeader';
import {DropdownContainer} from 'react-universal-ui';
import DropdownSetting from './dropdownSetting';
import DropdownQuestion from './dropdownQuestion';
import DropdownBullhorn from './dropdownBullhorn';
type Props = {};
export default function AccountHeader(props: Props) {
  return (
    <View style={styles.accountHeader}>
      <DropdownContainer
        dropdownComponent={DropdownSetting}
        dropdownOffset={{top: 0, left: 35}}>
        <ItemHeader img={assets.setting} />
      </DropdownContainer>
      <DropdownContainer
        dropdownComponent={DropdownQuestion}
        dropdownOffset={{top: 0, left: 10}}>
        <ItemHeader img={assets.question} />
      </DropdownContainer>
      <DropdownContainer
        dropdownComponent={DropdownBullhorn}
        dropdownOffset={{top: 0, left: 10}}>
        <ItemHeader img={assets.bullhorn} />
      </DropdownContainer>

      <Hover>
        {({focused}) => {
          return (
            <View
              style={{
                padding: 15,
                backgroundColor: focused ? colors.blue005A9E : null,
              }}>
              <TouchableOpacity activeOpacity={1}>
                <Text style={styles.profileName}> TT </Text>
              </TouchableOpacity>
            </View>
          );
        }}
      </Hover>
    </View>
  );
}
const styles = StyleSheet.create({
  accountHeader: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  profileName: {
    // borderWidth: 1,
    padding: 7,
    borderRadius: 50,
    color: colors.mainRight,
    backgroundColor: colors.redDA532C,
  },
});
