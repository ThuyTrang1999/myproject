import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {colors} from '../../../../ruui/colors';
import {Image, TextInput, TouchableOpacity} from 'react-native-web';
import {assets} from '../../../../ruui/assets';
import TitleWithImg from '../../../Component/itemMain/titleWithImg';
import Hover from '../../../Component/hover';
import MyDay from '../MainRight/myDay';
import Important from '../MainRight/important';
import WasPlanning from '../MainRight/wasPlanning';
import AssignedToYou from '../MainRight/assignedToYou';
import Action from '../MainRight/action';
type Props = {};

const useRenderComponent = (title) => {
  if (title === 'Ngày của tôi') {
    return <MyDay />;
  } else if (title === 'Quan trọng') {
    return <Important />;
  } else if (title === 'Đã lập kế hoạch') {
    return <WasPlanning />;
  } else if (title === 'Đã giao cho bạn') {
    return <AssignedToYou />;
  } else if (title === 'Tác vụ') {
    return <Action />;
  } else {
    return null;
  }
};
const menuConfigs = [
  {
    title: 'Ngày của tôi',
    img: assets.sun,
    titleColor: 'black',
  },
  {
    title: 'Quan trọng',
    img: assets.star,
    titleColor: colors.blue346fef,
  },
  {
    title: 'Đã lập kế hoạch',
    img: assets.calendar,
    titleColor: colors.blue346fef,
  },
  {
    title: 'Đã giao cho bạn',
    img: assets.user,
    titleColor: colors.green009900,
  },
  {
    title: 'Tác vụ',
    img: assets.home,
    titleColor: colors.blue346fef,
  },
];

const bottomIcon = [
  {key: assets.mail},
  {key: assets.calendar},
  {key: assets.people},
  {key: assets.paperclip},
  {key: assets.check},
];
export default function MainLeft(props: Props) {
  const [title, setTitle] = useState('Ngày của tôi');
  const [activeMenu, setActiveMenu] = useState(false);
  const renderRightLayout = useRenderComponent(title);
  return (
    <View style={styles.container}>
      <View
        style={[
          {justifyContent: 'space-between'},
          activeMenu ? {flex: 1} : {flex: 0.12},
        ]}>
        <View>
          <View>
            <TouchableOpacity
              style={{
                paddingVertical: 15,
                width: 'max-content',
                marginHorizontal: 5,
              }}
              onPress={() => setActiveMenu(!activeMenu)}
              activeOpacity={1}>
              <View>
                <Hover
                  colorStyle={{backgroundColor: 'white'}}
                  wrapperStyle={{backgroundColor: colors.grayf4f4f4}}>
                  <Image source={assets.bars} style={styles.iconStyle} />
                </Hover>
              </View>
            </TouchableOpacity>
          </View>
          {menuConfigs.map((item, index) => {
            const {title: titleRender, img, titleColor} = item;
            const active = title === item.title;
            return (
              <Hover key={index}>
                {({focused}) => {
                  const focusedStyle = focused ? '#F8F8FF' : colors.grayf4f4f4;
                  const isActive = active ? '#EEEEEE' : colors.grayf4f4f4;
                  return (
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => setTitle(item.title)}
                      style={{
                        backgroundColor: active ? isActive : focusedStyle,
                      }}>
                      <TitleWithImg
                        isActive={activeMenu}
                        title={titleRender}
                        img={img}
                        titleStyle={{
                          color: active ? titleColor : 'black',
                          fontWeight: active ? 500 : null,
                        }}
                      />
                    </TouchableOpacity>
                  );
                }}
              </Hover>
            );
          })}
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
            <Hover
              colorStyle={{backgroundColor: '#F8F8FF'}}
              wrapperStyle={{
                flexDirection: 'row',
                backgroundColor: colors.grayf4f4f4,
                paddingLeft: 5,
              }}>
              <TouchableOpacity activeOpacity={1}>
                <Image source={assets.plus} style={styles.iconStyle} />
              </TouchableOpacity>
              <TextInput
                style={{paddingRight: 30}}
                placeholder={'Danh sách mới'}
              />
            </Hover>
            <Hover
              colorStyle={{backgroundColor: '#F8F8FF'}}
              wrapperStyle={{
                backgroundColor: colors.grayf4f4f4,
                paddingHorizontal: 10,
              }}>
              <TouchableOpacity activeOpacity={1}>
                <Image source={assets.addgroup} style={styles.iconStyle} />
              </TouchableOpacity>
            </Hover>
          </View>
        </View>
        <View style={styles.bottomStyle}>
          {bottomIcon.map((item, index) => {
            return (
              <Hover
                colorStyle={{backgroundColor: '#EEEEEE'}}
                wrapperStyle={{backgroundColor: colors.grayf4f4f4}}>
                <TouchableOpacity activeOpacity={1}>
                  <Image
                    key={index}
                    source={item.key}
                    style={styles.iconStyle}
                  />
                </TouchableOpacity>
              </Hover>
            );
          })}
          <TouchableOpacity />
        </View>
      </View>
      <View style={{flex: 4}}>{renderRightLayout}</View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.grayf4f4f4,
  },
  iconStyle: {
    width: 15,
    height: 15,
    margin: 10,
  },
  leftMenuStyle: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  leftTitleStyle: {
    marginVertical: 10,
  },
  bottomStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
  },
});
