import React, {useState} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {colors} from '../../../ruui/colors';
import {ScrollView, TouchableOpacity} from 'react-native-web';
import {assets} from '../../../ruui/assets';
import Hover from '../../Component/hover';
import ItemDropDownSetting from '../itemHeader/itemDropDownSetting';

type Props = {close?: Function};
export default function DropdownSetting(props: Props) {
  const {close} = props;
  return (
    <ScrollView style={[styles.container, {maxHeight: 600, width: 320}]}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text style={styles.dropdownText}>Thiết đặt</Text>
        <View>
          <Hover>
            {({focused}) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    close();
                  }}
                  style={{
                    backgroundColor: focused ? colors.grayE1DFDD : null,
                  }}
                  activeOpacity={1}>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                      margin: 10,
                      alignContent: 'flex-end',
                    }}
                    source={assets.delete}
                  />
                </TouchableOpacity>
              );
            }}
          </Hover>
        </View>
      </View>
      <Text style={{marginTop: 15}}>Nhập từ Wunderlist</Text>
      <Text style={{marginTop: 10}}>Nhập danh sách và tác vụ của bạn</Text>
      <TouchableOpacity style={styles.btnNhap} activeOpacity={1}>
        Nhập
      </TouchableOpacity>
      <TouchableOpacity style={{marginTop: 10}} activeOpacity={1}>
        <Text style={{color: colors.blue346fef}}>
          Chia sẻ ý kiến phản hồi về quá trình nhập
        </Text>
      </TouchableOpacity>

      <Text style={[styles.dropdownText, {marginTop: 15}]}>Tổng quát</Text>

      <ItemDropDownSetting
        text={'Xác nhận trước khi xóa'}
        img={assets.toggle}
      />
      <ItemDropDownSetting text={'Thêm tác vụ mới ở đầu'} img={assets.toggle} />
      <ItemDropDownSetting
        text={'Di chuyển tác vụ gắn dấu sao lên đầu'}
        img={assets.toggle}
      />
      <ItemDropDownSetting
        text={'Phát âm thanh hoàn thiện'}
        img={assets.toggle}
      />
      <ItemDropDownSetting
        text={'Hiển thị menu bấm chuột phải'}
        img={assets.toggle}
      />
      <ItemDropDownSetting
        text={'Bật thông báo nhắc nhở'}
        img={assets.toggle}
      />
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 400,
    backgroundColor: colors.whiteFAF9F8,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  btnNhap: {
    width: 70,
    height: 30,
    flexDirection: 'row',
    backgroundColor: colors.blue346fef,
    borderRadius: 3,
    color: colors.mainRight,
    fontWeight: 'bold',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 10,
  },
  dropdownText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  imgDelete: {
    alignContent: 'flex-end',
    // marginLeft: 160,
  },
});
