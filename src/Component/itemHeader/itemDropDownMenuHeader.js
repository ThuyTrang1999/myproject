import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-web';
import Hover from '../../Component/hover';
import {colors} from '../../../ruui/colors';
type Props = {
  img?: string,
  img1?: string,
  text?: string,
  text1?: string,
};
export default function ItemDropDownMenuHeader(props: Props) {
  const {img, text, img1, text1} = props;
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 15,
      }}>
      <View>
        <Hover>
          {({focused}) => {
            return (
              <TouchableOpacity
                style={{
                  backgroundColor: focused ? colors.grayE1DFDD : null,
                  flexDirection: 'row',
                }}
                activeOpacity={1}>
                <Image
                  style={{
                    height: 30,
                    width: 30,
                    marginHorizontal: 10,
                  }}
                  source={img}
                />
                <Text style={{alignSelf: 'center'}}>{text}</Text>
              </TouchableOpacity>
            );
          }}
        </Hover>
      </View>

      <View>
        <Hover>
          {({focused}) => {
            return (
              <TouchableOpacity
                style={{
                  backgroundColor: focused ? colors.grayE1DFDD : null,
                  flexDirection: 'row',
                }}
                activeOpacity={1}>
                <Image
                  style={{
                    height: 30,
                    width: 30,
                    marginHorizontal: 10,
                  }}
                  source={img1}
                />
                <Text style={{alignSelf: 'center'}}>{text1}</Text>
              </TouchableOpacity>
            );
          }}
        </Hover>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
