import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {colors} from '../../../ruui/colors';
import Hover from '../hover';
type Props = {
  img?: string,
  title?: string,
};
export default function TitleMainTextRight(props: Props) {
  const {img, title} = props;
  return (
    <Hover>
      {({focused}) => {
        return (
          <View
            style={[
              styles.styleHover,
              {backgroundColor: focused ? colors.grayf4f4f4 : '#fff'},
            ]}>
            <TouchableOpacity activeOpacity={1} style={{flexDirection: 'row'}}>
              <Image source={img} style={styles.styleImg} />
              <Text style={{alignSelf: 'center'}}>{title}</Text>
            </TouchableOpacity>

            {/*<TitleMainRight img={assets.light} title={'Hôm nay'} />*/}
          </View>
        );
      }}
    </Hover>
  );
}
const styles = StyleSheet.create({
  styleImg: {
    width: 15,
    height: 15,
    marginTop: 3,
    marginRight: 10,
  },
  styleHover: {
    marginRight: 10,

    padding: 8,

  },
});
