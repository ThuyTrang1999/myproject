export const checkMonth = (month) => {
  if (
    month === 1 ||
    month === 3 ||
    month === 5 ||
    month === 7 ||
    month === 8 ||
    month === 10 ||
    month === 12
  ) {
    return 1;
  }

  if (
    month === 4 ||
    month === 6 ||
    month === 9 ||
    month === 9 ||
    month === 11
  ) {
    return 2;
  }

  if (month === 2) {
    return 0;
  }
  return -1;
};
export const checkDay = (day, month, year, check) => {
  if (!check) {
    if (day === 1) {
      month = month - 1;
      if (checkMonth(month) === 1) {
        return {yearChange: year, monthChange: month, days: 31};
      }
      if (checkMonth(month) === 2) {
        return {yearChange: year, monthChange: month, days: 30};
      }
      if (checkMonth(month) === 0) {
        return {yearChange: year, monthChange: month, days: 28};
      }
      if (checkMonth(month) === -1) {
        return {yearChange: year - 1, monthChange: 12, days: 31};
      }
    } else {
      return {yearChange: year, monthChange: month, days: day - 1};
    }
  } else if (check) {
    if (checkMonth(month + 1) === -1) {
      if (day === 31) {
        return {yearChange: year + 1, monthChange: 1, days: 1};
      }
    }
    if (checkMonth(month) === 1) {
      if (day === 31) {
        return {yearChange: year, monthChange: month + 1, days: 1};
      }
    }
    if (checkMonth(month) === 2) {
      if (day === 30) {
        return {yearChange: year, monthChange: month + 1, days: 1};
      }
    }
    if (checkMonth(month) === 0) {
      if (day === 28) {
        return {yearChange: year, monthChange: month + 1, days: 1};
      }
    }
    if (checkMonth(month) === -1) {
      return {yearChange: year + 1, monthChange: 1, days: 1};
    } else {
      return {yearChange: year, monthChange: month, days: day + 1};
    }
  }
};
