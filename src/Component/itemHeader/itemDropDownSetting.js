import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import Hover from '../../Component/hover';
import {TouchableOpacity} from 'react-native-web';
type Props = {
  text?: string,
  img?: string,
};
export default function ItemDropDownSetting(props: Props) {
  const {text, img} = props;
  return (
    <View>
      <Text style={{marginTop: 15, fontWeight: 'bold'}}>{text}</Text>
      <View style={{flexDirection: 'row'}}>
        <Hover>
          {({focused}) => {
            return (
              <TouchableOpacity
                style={
                  {
                    // backgroundColor: focused ? colors.grayE1DFDD : null,
                  }
                }
                activeOpacity={1}>
                <Image
                  style={{
                    height: 40,
                    width: 40,
                    marginHorizontal: 10,
                    alignContent: 'flex-end',
                  }}
                  source={img}
                />
              </TouchableOpacity>
            );
          }}
        </Hover>

        <Text style={{marginTop: 10}}>Bật</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
