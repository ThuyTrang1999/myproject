import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import type {Style} from '../typeDefinition';
type Props = {
  children?: Function,
  wrapperStyle?: Style,
  colorStyle?: Style,
};
export default function Hover(props: Props) {
  const {wrapperStyle, children, colorStyle} = props,
    [focused, setFocused] = useState(false);
  return (
    <View
      activeOpacity={1}
      onMouseEnter={() => setFocused(true)}
      onMouseLeave={() => setFocused(false)}
      style={[wrapperStyle, focused && colorStyle]}>
      {typeof children === 'function' ? children({focused}) : children}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {},
});
