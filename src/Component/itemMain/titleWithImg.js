import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import Hover from '../../component/hover';
import type {Style} from '../../typeDefinition';
type Props = {
  img?: string,
  title?: string,
  isHover?: Style,
  isNonHover?: Style,
  titleStyle?: Style,
  isActive?: boolean,
};
export default function TitleWithImg(props: Props) {
  const {img, title, isHover, isNonHover, titleStyle, isActive} = props;
  return (
    <Hover colorStyle={isHover} wrapperStyle={isNonHover}>
      <View style={styles.leftMenuStyle}>
        <Image source={img} style={styles.iconStyle} />
        {isActive && (
          <Text style={[styles.leftTitleStyle, titleStyle]}>{title}</Text>
        )}
      </View>
    </Hover>
  );
}
const styles = StyleSheet.create({
  leftMenuStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
  },
  iconStyle: {
    width: 15,
    height: 15,
    marginVertical: 15,
  },
  leftTitleStyle: {
    padding: 11,
  },
});
