import React, {useRef, useState} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {colors} from '../../../../ruui/colors';
import TitleMainRight from '../../../Component/itemMain/titleMainRight';
import {assets} from '../../../../ruui/assets';
import {FlatList, TextInput} from 'react-native-web';
import Calendar from 'react-calendar';
import ItemImprotant from '../../../Component/itemMain/itemImprotant';

type Props = {};
export default function Important(props: Props) {
  const [value, setValue] = useState('');
  const [isActive, setIsActive] = useState(false);

  const [date, setDate] = useState(new Date());
  const onChange = () => {
    setDate(date);
  };
  const dataList = [
    {
      time: '12:00',
      date: '2020-10-07',
      description: 'Ting Ting',
    },
    {
      time: '1:00',
      date: '2020-10-07',
      description: 'Ting Ting',
    },
    {
      time: '21:00',
      date: '2020-10-07',
      description: 'Ting Ting',
    },
  ];
  const onButtonClick = () => {
    if (!isActive) {
      textInputPlus.current.focus();
    }
  };
  const addItem = () => {
    if (value) {
      console.log('a');
    }
  };
  const textInputPlus = useRef(null);
  return (
    <View style={styles.container}>
      <View style={{flex: 1}}>
        <TitleMainRight
          title={'Quan trọng'}
          img={assets.dot}
          titleColor={{color: colors.blue346fef}}
        />
      </View>
      <View style={{flex: 5}}>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 15,
            borderBottomWidth: 1,
            borderBottomColor: colors.grayE1DFDD,
          }}>
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
            }}>
            <TouchableOpacity
              onPress={() => {
                onButtonClick();
                setIsActive(!isActive);
              }}>
              <Image
                source={isActive ? assets.oval : assets.plus}
                style={styles.styleImg}
              />
            </TouchableOpacity>
            <TextInput
              value={value}
              onChangeText={(textInputValue) => setValue(textInputValue)}
              ref={textInputPlus}
              style={[
                styles.textValue,
                {
                  color: colors.gray767678,
                  paddingHorizontal: 12,
                },
              ]}
              placeholder={'Thêm tác vụ'}
            />
          </View>
          <View style={{justifyContent: 'flex-end'}}>
            <TouchableOpacity
              onPress={() => {
                addItem();
              }}
              onChangeText={() => setValue(value)}
              style={{color: 'black', display: value ? 'block' : 'none'}}>
              THÊM
            </TouchableOpacity>
          </View>
        </View>
        {/*//////////////////*/}
        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              flex: 2.5,
              paddingVertical: 15,
            }}>
            <Calendar onChange={onChange} value={date} />
            {date.toString()}
          </View>

          <View
            style={{
              flex: 3.5,
              paddingVertical: 15,
            }}>
            <FlatList
              style={{}}
              data={dataList}
              keyExtractor={(item, index) => 'key' + index}
              renderItem={({item}) => {
                return (
                  <ItemImprotant
                    time={item.time}
                    desc={item.description}
                    date={item.date}
                    img={assets.freeTime}
                  />
                );
              }}
            />
          </View>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    backgroundColor: colors.mainRight,
  },
  styleImg: {
    width: 15,
    height: 15,
    marginLeft: 20,
    marginTop: 5,
  },
});
