import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import Hover from '../../Component/hover';
import {TouchableOpacity} from 'react-native-web';
import {colors} from '../../../ruui/colors';
import {assets} from '../../../ruui/assets';
type Props = {
  close?: Function,
};
export default function DropdownBullhorn(props: Props) {
  const {close} = props;
  return (
    <View style={[styles.container, {maxHeight: 600, width: 300}]}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.dropdownText}>Có gì mới</Text>
        <View style={{marginLeft: 150}}>
          <Hover>
            {({focused}) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    close();
                  }}
                  style={{
                    backgroundColor: focused ? colors.grayE1DFDD : null,
                  }}
                  activeOpacity={1}>
                  <Image
                    style={{
                      height: 15,
                      width: 15,
                      margin: 10,
                      alignContent: 'flex-end',
                    }}
                    source={assets.delete}
                  />
                </TouchableOpacity>
              );
            }}
          </Hover>
        </View>
      </View>
      <TouchableOpacity style={{marginTop: 15}} activeOpacity={1}>
        <Image
          style={{
            height: 300,
            width: 250,
            marginHorizontal: 10,
            alignContent: 'flex-end',
          }}
          source={assets.imgApptodo}
        />
      </TouchableOpacity>
      <Text>To Do giờ đã sẵn cho máy Mac</Text>
      <TouchableOpacity style={styles.btnDownload} activeOpacity={1}>
        Tải xuống
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: 400,
    backgroundColor: colors.whiteFAF9F8,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  btnDownload: {
    width: 200,
    height: 30,
    flexDirection: 'row',
    backgroundColor: colors.blue346fef,
    borderRadius: 3,
    color: colors.mainRight,
    fontWeight: 'bold',
    alignItems: 'center',
    paddingHorizontal: 65,
    marginTop: 15,
  },
  dropdownText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  imgDelete: {
    alignContent: 'flex-end',
    // marginLeft: 160,
  },
  textCapNhat: {
    alignSelf: 'center',
    padding: 15,
  },
});
