import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {assets} from '../../../../ruui/assets';
import {colors} from '../../../../ruui/colors';
import TitleMainRight from '../../../Component/itemMain/titleMainRight';
import {Image} from 'react-native-web';
type Props = {};
export default function AssignedToYou(props: Props) {
  return (
    <View style={styles.container}>
      <View >
        <TitleMainRight
          title={'Đã giao cho bạn'}
          img={assets.dot}
          titleColor={{color: colors.green009900}}
        />
      </View>
      <View>
        <Image style={{width: 400, height: 400, alignSelf:'center', marginVertical:60}} source={assets.assigned} />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainRight,
    padding: 20,
  },
});
