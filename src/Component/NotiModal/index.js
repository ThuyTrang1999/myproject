import React, {useCallback, useContext, useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  FlatList,
  Dimensions,
} from 'react-native';
import {ActionContext} from '../../res/DataContext';
import {TouchableOpacity} from 'react-native-web';
import {colors} from '../../../ruui/colors';
import Modal from 'modal-react-native-web';
type Props = {};

const windowsWidth = Dimensions.get('window').width;
const windowsHeight = Dimensions.get('window').height;
export default function NotiModal(props: Props) {
  let day = new Date();
  let date =
    day.getDate() + '-' + (day.getMonth() + 1) + '-' + day.getFullYear();
  let time = day.getHours() + ':' + day.getMinutes() + ':' + day.getSeconds();
  let dateTime = date + ' ' + time;
  const {contextEdit, contextList, contextModal} = useContext(ActionContext);
  const [modalVisible, setModalVisible] = contextModal;
  const [editItem, setEditItem] = contextEdit,
    [stateCont, setStateCont] = contextList,
    [textInput, setTextInput] = useState(''),
    addContext = useCallback(
      (item) => {
        if (item.length > 0) {
          const obj = {
            id: Math.floor(Math.random() * 1000) + 1,
            text: item,
            time: dateTime,
          };
          const arr = [...stateCont];
          arr.push(obj);
          setStateCont(arr);
          console.log(arr);
          setTextInput('');
        } else {
          alert('K dc để trống');
        }
      },
      [dateTime, setStateCont, stateCont],
    );
  const hideDialog = () => {
    setModalVisible(false);
  };

  const deleteContext = useCallback(
    (id) => {
      const result = window.confirm('Xóa à ?');
      if (result) {
        setStateCont(stateCont.filter((e) => e.id !== id));
      }
    },
    [setStateCont, stateCont],
  );
  const onEditItem = useCallback(
    (editText) => {
      const nextTodoList = [...stateCont],
        editItemIndex = nextTodoList.findIndex((e) => e.id === editItem);
      if (editItemIndex >= 0) {
        nextTodoList[editItemIndex].text = editText;
        setStateCont(nextTodoList);
      }
      hideDialog();
    },
    [stateCont, editItem],
  );

  return (
    <View style={{flex: 5}}>
      <View
        style={{
          flexDirection: 'row',
          paddingVertical: 15,
          borderBottomWidth: 1,
          borderBottomColor: colors.grayE1DFDD,
        }}>
        <View
          style={{
            flexDirection: 'row',
            flex: 6,
          }}>
          <TextInput
            style={{
              color: colors.gray767678,
              paddingHorizontal: 12,
              width: '100%',
            }}
            placeholder={'Thêm tác vụ'}
            value={textInput}
            onChangeText={(textInputValue) => {
              setTextInput(textInputValue);
            }}
            onSubmitEditing={() => addContext(textInput)}
          />
        </View>
        <View style={{justifyContent: 'flex-end'}}>
          <TouchableOpacity
            onPress={() => {
              addContext(textInput);
              console.log(textInput);
            }}
            style={{color: 'black', display: textInput ? 'block' : 'none'}}>
            <Text>THÊM</Text>
          </TouchableOpacity>
        </View>
      </View>
      <FlatList
        data={stateCont}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={({item}) => {
          const {id} = item;
          return (
            <View
              style={{
                flexDirection: 'row',
                paddingVertical: 15,
                borderBottomWidth: 1,
                borderBottomColor: colors.grayE1DFDD,
              }}>
              <View
                style={{
                  // flexDirection: 'row',
                  flex: 1,
                }}>
                <Text>{item.text}</Text>
              </View>
              <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => {
                    setModalVisible(true);
                    setEditItem(id);
                  }}>
                  <Text style={{color: 'green', paddingRight: 20}}>Sửa</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    deleteContext(id);
                  }}>
                  <Text style={{color: 'red'}}>Xóa</Text>
                </TouchableOpacity>
              </View>
            </View>
          );
        }}
      />
      <EditModal
        modalVisible={modalVisible}
        modalInitialValue={stateCont.find((e) => e.id === editItem)?.text}
        onEdit={onEditItem}
        onCancel={hideDialog}
      />
    </View>
  );
}
function EditModal({modalVisible, modalInitialValue, onEdit, onCancel}) {
  const [inputValue, setInputValue] = useState(modalInitialValue);

  React.useEffect(() => {
    setInputValue(modalInitialValue);
  }, [modalInitialValue]);

  return (
    <Modal visible={modalVisible} animationType="fade" transparent={true}>
      <View
        style={{
          width: windowsWidth,
          height: windowsHeight,
          backgroundColor: '#000',
          opacity: 0.1,
          margin: 'auto',
          position: 'absolute',
        }}
      />
      <View style={styles.viewModal}>
        <Text
          style={{
            alignSelf: 'center',
            fontWeight: 'bold',
            fontSize: 30,
          }}>
          Edit
        </Text>
        <TextInput
          style={styles.editText}
          // placeholder={'Tác vụ'}
          onChangeText={(text) => setInputValue(text)}
          value={inputValue}
          onSubmitEditing={() => onEdit(inputValue)}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            style={[styles.btnEdit, {backgroundColor: colors.blue346fef}]}
            onPress={() => onEdit(inputValue)}
            activeOpacity={1}>
            <Text style={{color: colors.mainRight}}>Cập nhật</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onCancel}
            style={styles.btnEdit}
            activeOpacity={1}>
            <Text style={{color: colors.mainRight}}>Thoát</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textValue: {
    width: '100%',
  },
  viewModal: {
    marginHorizontal: 400,
    marginVertical: 200,
    backgroundColor: colors.mainRight,
    padding: 30,
  },
  editText: {
    color: colors.gray767678,
    paddingHorizontal: 12,
    borderBottomWidth: 1,
    borderBottomColor: colors.grayE1DFDD,
    paddingVertical: 15,
  },
  btnEdit: {
    // alignSelf: 'center',
    borderWidth: 1,
    borderColor: colors.blueC3ddf7,
    padding: 10,
    marginTop: 20,
    backgroundColor: 'red',
    shadowColor: '#c6c2c2',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
  styleImgs: {
    width: 15,
    height: 15,
    marginTop: 3,
    marginRight: 10,
  },
  styleHover: {
    marginRight: 10,

    padding: 8,
  },
});
