import React from 'react';
import {StyleSheet, View} from 'react-native';
import Header from '../header';
import Body from '../main';
type Props = {};
export default function index(props: Props) {
  return (
    <View style={styles.container}>
      <Header />
      <Body />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
