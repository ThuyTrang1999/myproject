import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import Hover from '../../Component/hover';
import {colors} from '../../../ruui/colors';
import {TouchableOpacity} from 'react-native-web';
type Props = {
  img?: string,
};
export default function ItemHeader(props: Props) {
  const {img} = props;
  return (
    <Hover>
      {({focused}) => {
        return (
          <View
            style={{
              padding: 15,
              backgroundColor: focused ? colors.blue005A9E : null,
            }}>
            <TouchableOpacity activeOpacity={1}>
              <Image
                style={{
                  width: 20,
                  height: 20,
                }}
                source={img}
              />
            </TouchableOpacity>
          </View>
        );
      }}
    </Hover>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
