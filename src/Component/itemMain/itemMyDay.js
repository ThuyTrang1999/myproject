import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import {colors} from '../../../ruui/colors';
import Modal from 'modal-react-native-web';

type Props = {
  text?: string,
  key?: string,
};
export default function ItemMyDay(props: Props) {
  const {text} = props;
  const windowsWidth = Dimensions.get('window').width;
  const windowsHeight = Dimensions.get('window').height;
  const [modalVisible, setmodalVisible] = useState(false);
  const showDialog = () => {
    setmodalVisible(true);
  };
  const hideDialog = () => {
    setmodalVisible(false);
  };
  return (
    <View
      style={{
        flexDirection: 'row',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: colors.grayE1DFDD,
      }}>
      <View
        style={{
          flexDirection: 'row',
          flex: 1,
        }}>
        <Text>{text}</Text>
        {/*<Text>{date}</Text>*/}
      </View>
      <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={() => {
            showDialog();
          }}>
          <Text style={{color: 'green', paddingRight: 20}}>Sửa</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}}>
          <Text style={{color: 'red'}}>Xóa</Text>
        </TouchableOpacity>
      </View>
      <Modal visible={modalVisible} animationType="fade" transparent={true}>
        <View
          style={{
            width: windowsWidth,
            height: windowsHeight,
            backgroundColor: '#000',
            opacity: 0.5,
            margin: 'auto',
            position: 'absolute',
          }}
        />
        <View style={styles.viewModal}>
          <Text style={{alignSelf: 'center', fontWeight: 'bold', fontSize: 30}}>
            Edit
          </Text>
          <TextInput style={styles.editText} placeholder={'Tác vụ'} />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              style={[styles.btnEdit, {backgroundColor: colors.blue346fef}]}
              activeOpacity={1}>
              <Text style={{color: colors.mainRight}}>Cập nhật</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                hideDialog();
              }}
              style={styles.btnEdit}
              activeOpacity={1}>
              <Text style={{color: colors.mainRight}}>Thoát</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}
const styles = StyleSheet.create({
  viewModal: {
    marginHorizontal: 400,
    marginVertical: 200,
    backgroundColor: colors.mainRight,
    padding: 30,
  },
  editText: {
    color: colors.gray767678,
    paddingHorizontal: 12,
    borderBottomWidth: 1,
    borderBottomColor: colors.grayE1DFDD,
    paddingVertical: 15,
  },
  btnEdit: {
    // alignSelf: 'center',
    borderWidth: 1,
    borderColor: colors.blueC3ddf7,
    padding: 10,
    marginTop: 20,
    backgroundColor: 'red',
    shadowColor: '#c6c2c2',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
});
