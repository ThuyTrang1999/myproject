import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {colors} from '../../../ruui/colors';
import {Image} from 'react-native-web';
type Props = {
  time?: string,
  desc?: string,
  date?: string,
  img?: string,
};
const d = new Date();

const checkTime = (props) => {
  const {time} = props;
  if (d.getHours() > time.split(':', 1)) {
    return true;
  }
  return false;
};
export default function ItemImprotant(props: Props) {
  const {time, desc, date, img} = props;
  return (
    <View style={checkTime(props) ? styles.af : styles.aB}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          marginVertical: 10,
        }}>
        <Text style={{color: colors.mainRight}}>{time}</Text>
        <Image style={{width: 20, height: 20}} source={img} />
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <Text style={{color: colors.mainRight}}>{date}</Text>
        <Text style={{color: colors.mainRight}}>{desc}</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  af: {
    backgroundColor: colors.blue346fef,
    width: 200,
    // height:100,
    marginVertical: 20,
    marginHorizontal: 100,
    borderRadius: 10,
  },
  aB: {
    backgroundColor: '#ff77ff',
    width: 200,
    marginVertical: 20,
    marginHorizontal: 100,
    borderRadius: 10,
  },
});
