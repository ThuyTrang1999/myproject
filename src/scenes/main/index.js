import React from 'react';
import {StyleSheet, View} from 'react-native';
import MainLeft from './MainLeft';
type Props = {};
export default function Body(props: Props) {
  return (
    <View style={styles.container}>
      <View style={styles.styleMainLeft}>
        <MainLeft />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 12,
    flexDirection: 'row',
  },
  styleMainLeft: {
    flex: 1,
  },
  styleMainRight: {
    flex: 4,
  },
});
